# Laravel Admin LTE 3
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-7-admin-lte-3.git`
2. Go inside the folder: `cd laravel-7-admin-lte-3`
3. Run `php artisan serve`
4. Open your favorite browser: http//localhost:8000/admin

### Screen shot

Admin LTE Starter Page

![Admin LTE Starter Page](img/adminlte.png  "Admin LTE Starter Page")
